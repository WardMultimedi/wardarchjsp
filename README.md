# wardArch-JSP
A standardized arch for my (JSP-web) projects

## Goals
To create a project ready for coding this arch provides:

 - standard file-structure (test & main)
 - using java 11 including plugins for java 11
 - newer version of Junit(jupiter) for java 11
 - a basic startpoint for creating a website using JSP

## To install 
use `mvn clean install archetype:update-local-catalog`
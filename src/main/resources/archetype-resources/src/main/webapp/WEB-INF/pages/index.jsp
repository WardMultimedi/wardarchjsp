<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Hello index</title>

    <!-- Bootstrap 3 minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- my CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- jQuery minified library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap 3 minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <h1>Hello index</h1>
    <p>This is a sample index page. Work in progress...</p>
</div>
</body>
</html>
